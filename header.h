#include <winsock2.h>
#include <string>
#include<iostream>
#include <map>

struct header
{
public:
  int size;
  int to,from;
};

std::ostream& operator<< (std::ostream& s, const header& h){
  char tmp[3];
  for (size_t i=0; i<sizeof(header); ++i){
    sprintf(tmp, "%02X ", ((const char*)&h)[i]);
    s << tmp;
  }
  return s;
}

class message
{
public:
    header h;
    std::string body;
	//message(int t, int f, std::string s): h.to(t), h.from(f), body(s), h.size(s.lenght() ){}
	message(int t, int f, std::string s)
	{
		h.to=t;
		h.from=f;
		body=s; 
		h.size=s.length();
	}
  
  message()
	{
		h.to=0;
		h.from=0;
		body=""; 
		h.size=0;
	}

};

int recvnmessage(message& msg, SOCKET sock){
  int RetV = recv(sock, (char*)&(msg.h) , sizeof(header), 0); //Recieving header   
  std::cerr << msg.h << "\n";

  if(RetV == SOCKET_ERROR)
    {
        printf("Unable to recv header\n");
       return 0;
    }

 
  char *buf = (char*) malloc(msg.h.size+1); //creating buffer
  RetV = recv(sock, buf, msg.h.size, 0);				//Recieving body
  std::cout<<RetV << " of " << msg.h.size << "\n";
  if(RetV == SOCKET_ERROR)
    {
        printf("Unable to recv body\n");
        return 0;
    }
  buf[msg.h.size]='\0';							//Adding null character
  msg.body = std::string(buf);
  delete [] buf;

  return RetV;
}

/*int recvnmessage1(message& msg, SOCKET sock){
  //int RetV = recv(sock, (char*)(&msg.h) , sizeof(header), 0);
  char *buf = (char*) malloc(msg.h.size+sizeof(header));		
  int RetV=recv(sock, buf, msg.h.size+sizeof(header), 0);   
	//printf("%s", buf);
	buf[msg.h.size]='\0';
	msg.h = *((header*)buf);
	msg.body = std::string( (char*) (buf+sizeof(header)) );
	//printf("test %s", (char*)(buf+sizeof(header)) );
  //buf[msg.h.size]='\0';
  //msg.body = std::string(buf);
  free (buf);
  return RetV;
}*/

int sendmessage(const message& msg, SOCKET sock){
  std::cerr << "sending " << msg.h;
  char *buf = (char*)malloc(msg.h.size + sizeof(header));    // Creating buffer
  memcpy(buf, (void*)&(msg.h), sizeof(header));                  // Put header into buffer
  memcpy(buf+sizeof(header), msg.body.c_str(), msg.h.size);  // Put mesaage body into buffer 
															 // without null character
  //buf[msg.h.size + sizeof(header)]='\0';
  //printf("\n%s\n", buf + sizeof(header) );
  //printf("\n%d\n", msg.h.size );
  //printf("\n test%d \n", msg.h.size + sizeof(header) );
  return send(sock, buf, msg.h.size + sizeof(header), 0);  // send buffer
  free(buf);
}

class rwLock{
};

struct user{
  int uid;
  SOCKET handle;
  std::string name;

  user(int id, std::string s, SOCKET h)
  {
    uid = id;
    name = s;
    handle = h;
  }

  user()
  {
    uid = 0;
    name = "";
    handle = 0;
  }
};

bool operator < (const user& a, const user& b){
  return a.name < b.name;
}

typedef std::map<int, user> userlist;

struct clients{
 static  userlist list;

  static void print()
  {
    std::cout<<"\nUser list\n";
    userlist::iterator it;
    for(it = list.begin(); it!=list.end(); it++)
    {
      std::cout<<"\n"<<(*it).second.name<<" No."<<(*it).first<<"\n";
    }
  }
};
